#!/bin/bash

##Poetry Installation
python3 -m pip install --user pipx

exec $SHELL

python3 -m pipx ensurepath

pipx install poetry==1.2.0
