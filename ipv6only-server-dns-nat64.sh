#!/bin/bash

echo 'nameserver 2001:67c:2b0::4' >>/etc/resolvconf/resolv.conf.d/head
echo 'nameserver 2001:67c:2b0::6' >>/etc/resolvconf/resolv.conf.d/head

sudo systemctl status resolvconf.service
sudo systemctl restart systemd-resolved.service
