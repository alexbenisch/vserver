# tmux Prerequisits
- have tmux installed
- clone tmux configuration file into .config/tmux/tmux.conf
- start tmux without arguments
- detach from session by CTRL+b d
- run tmux source ~/.config/tmux/tmux.conf
- attach to session and run CTRL+b SHIFT+i for install
- Installation of plugins should run and we are set.

